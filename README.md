# Memories/Planning Board

Welcome to Memories/Planning Board! This web application allows users to create posts with pictures and text,
associating them with specific dates. The posts are then displayed chronologically and categorized into two
sections: future/plans or past/memories.

## Getting Started

To run the project, follow these steps:
1. Navigate to the project directory in your terminal.
2. Run the backend server by executing the following command:
   ```bash
   node src/assets/js/server.js
   ```
3. Open the `index.html` file in your web browser.

## Interface Design and Prototype

The interface design and prototype for this project were created using Figma. <br>
Link to see the design and the prototype: [Memories/Planning Board](https://www.figma.com/file/rtkiTQpz2MrPfR7WHJ55Gr/My-board-UI?type=design&node-id=0%3A1&mode=design&t=Z546nfCpXoyTndeC-1)

### Screenshot of the Design
![Screenshot of the Design](screenshots/my-board-ui-design.png)

### Screenshot of the Prototype
![Screenshot of the Prototype](screenshots/my-board-ui-prototype.png)

## Scripts

- `npm install`: This script installs the project's dependencies.
- `npm run server`: This script starts the backend server.
- `npm run lint`: This script runs ESLint and Stylelint to check the code for any linting errors.
- `npm run sass`: This script compiles the SCSS files into CSS.
- `npm run test`: This script runs the tests for the project.

## Features

- **Create Posts:** Users can create posts by adding pictures and text, associating them with specific dates.
- **Chronological Display:** Posts are displayed in chronological order, making it easy to track events over time.
- **Categorized Posts:** Posts are divided into two categories - future/plans and past/memories - providing a clear
  distinction between upcoming events and experiences.

## Technologies

- **Frontend:** HTML, CSS, JavaScript
- **Backend:** Node.js
- **Database:** JSON

```