const express = require('express');
const cors = require('cors');
const postsData = require("../posts/posts.json");
const {writeFileSync} = require("fs");

const app = express();

function sortPosts() {
    postsData.sort((a, b) => new Date(a.date) - new Date(b.date));

    return postsData;
}

app.use(cors());
app.use(express.json());
app.use(express.static('assets/posts'));
app.use(express.static('assets/img'));

app.post('/posts', (req, res) => {
    const postsData = sortPosts();
    postsData.push(req.body);
    writeFileSync("posts.json",
        JSON.stringify(postsData, null, 2));
    res.json(req.body);
});

app.get('/posts', (req, res) => {
    const postsData = require('../posts/posts.json');
    res.json(postsData);
});

app.get('/img', (req, res) => {
    const postsData = require('../img/posts/');
    res.json(postsData);
});

// Start the server
const port = 3000;
app.listen(port, () => {
    console.log(`Server is running at http://localhost:${port}`);
});
