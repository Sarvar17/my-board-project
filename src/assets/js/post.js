const formEl = document.querySelector('#form');

formEl.addEventListener('submit', () => {
    const data = {
        title: document.querySelector('#title').value,
        category: document.querySelector('#category').value,
        date: document.querySelector('#date').value,
        text: document.querySelector('#content').value,
        imagePost: document.querySelector('#image').value,
    };

    fetch('http://localhost:3000/posts', {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json'
        },
        body: JSON.stringify(data)
    })
        .then(res => res.json())
        .then(data => console.log(data))
        .catch(error => console.log(error))
});