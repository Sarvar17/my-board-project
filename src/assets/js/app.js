"use strict";

fetch("http://localhost:3000/posts")
    .then((response) => response.json())
    .then((data) => {
        renderPosts(data);
    })
    .catch((error) => console.error("Error fetching data:", error));

function renderPosts(posts) {
    const postTemplate = document.getElementById('post-template').innerHTML;
    const postContainer = document.querySelector(".content-block__posts");

    posts.forEach((post) => {
        const filledTemplate = fillTemplate(postTemplate, post);
        const newPostElement = createPostElement(filledTemplate);
        postContainer.appendChild(newPostElement);
    });
}

function fillTemplate(template, data) {
    return template.replace(/{{\s*(\w+)\s*}}/g, (match, key) => {
        return data[key] || match;
    });
}

function createPostElement(html) {
    const newPostDiv = document.createElement("div");
    newPostDiv.className = "post__item";

    newPostDiv.innerHTML = html;

    return newPostDiv;
}
