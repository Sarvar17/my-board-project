/* eslint-disable no-unused-vars */
function search() {
    let input, filter, divPosts, divPost, h2, i, txtValue;
    input = document.getElementById("search");
    filter = input.value.toUpperCase();
    divPosts = document.getElementById("posts");
    divPost = divPosts.getElementsByClassName("post__item");
    for (i = 0; i < divPost.length; i++) {
        h2 = divPost[i].getElementsByClassName("post__title")[0];
        txtValue = h2.textContent || h2.innerText;
        if (txtValue.toUpperCase().indexOf(filter) > -1) {
            divPost[i].style.display = "";
        } else {
            divPost[i].style.display = "none";
        }
    }
}

/* eslint-disable no-unused-vars */
function filter() {
    let selectElement = document.getElementById("filter-dropdown");
    let category = selectElement.value;

    let divPosts, divPost, i;
    divPosts = document.getElementById("posts");
    divPost = divPosts.getElementsByClassName("post__item");

    if (category === "all") {
        for (i = 0; i < divPost.length; i++) {
            divPost[i].style.display = "";
        }
    } else {
        for (i = 0; i < divPost.length; i++) {
            let childPosts = divPost[i].getElementsByClassName(category);
            if (childPosts.length > 0) {
                divPost[i].style.display = "";
            } else {
                divPost[i].style.display = "none";
            }
        }
    }
}
